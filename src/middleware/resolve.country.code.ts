import * as jwt from "jsonwebtoken"  

export const resolveCountryCode = (req: any, res: any, next: any) => { 
    const decode = jwt.decode(req.headers["authorization"].split(' ')[1])
    console.log(decode)
    req.body.countryCode = (decode as any).countryCode
    next()
};