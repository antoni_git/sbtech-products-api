import * as crypto from "crypto"
import { UserService } from "../services/user.service";

export function verifyUser(userService: UserService){
    return (req: any, res: any, next: any) => {
        console.log("Finding user")
        userService.findByUsername(req.body.username)
            .then((user) => {
                    console.log("VERIFYING")
                    let passwordFields = user.password.split("$");
                    let salt = passwordFields[0];
                    let hash = crypto.createHmac("sha512", salt).update(req.body.password).digest("base64");
                    if (hash === passwordFields[1]) {
                        req.body = Object.assign(req.body, {
                            userId: user._id                            
                        }) 
                        console.log("Verified")
                        return next();
                    } else {
                        return res.status(400).send({errors: ["Invalid username or password"]});
                    }
            }).catch((error) => {
                return res.status(400).send({errors: [error]}); 
        })};
}