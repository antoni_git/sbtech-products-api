import * as mongoose from "mongoose"

const OrderSchema = new mongoose.Schema({
    date: {type: Date, required: true },
    products: {type: [String] },
    status: {type: String, required: true,
         enum: ["Pending", "Processing", "Delivered", "Cancelled"]}
})

export interface IOrder extends mongoose.Document{
    date: Date
    products: [string]
    status: Status
}

export enum Status {
    Pending = "Pending",
    Processing = "Processing",
    Delivered = "Delivered",
    Cancelled = "Cancelled"
}


export const OrderModel = mongoose.model("Orders", OrderSchema);