import { Request, Response, Router, NextFunction, response } from "express";
import { OrderService } from "../services/order.service";
import { validJWTNeeded } from "../middleware/valid.token.needed";
import { check } from "express-validator"
import { validateResult } from "../middleware/validate.result";

export class OrderControlloer {
    private router: Router

    constructor(private orderService: OrderService){
        this.initRoutes()
    }

    private initRoutes(){
        this.router = Router()
        this.router.get("/", validJWTNeeded, (req: Request, res: Response, next: NextFunction) => { this.getAllOrders(req, res, next) })
        this.router.post("/",[ 
            check('products').exists(),
            check('status').exists(), validateResult, validJWTNeeded, 
            (req: Request, res: Response, next: NextFunction) => { this.addOrder(req, res, next) }])
        this.router.patch("/:id",[ 
            check('status').exists(), validateResult, validJWTNeeded,
             (req: Request, res: Response, next: NextFunction) => { this.changeOrderStatus(req, res, next) }])
    }

    /*
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
    */
    async getAllOrders(req: Request, res: Response, next: NextFunction) {
        await this.orderService.getAllOrders().then((result) => {
            res.status(200).send(result)
        }).catch((err) => {
            res.status(500).send({error: err})
        })
    }
     /*
        Add an Order
        Request Model:
            {
            "products" : ["productId", "productId2"],
            "status" : "Pending"
            }
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
    */
    async addOrder(req: Request, res: Response, next: NextFunction) {
        await this.orderService.addOrder({...req.body}).then(() => {
            res.status(200).send({message: "Order added successfully"})
        }).catch((err) => {
            res.status(500).send({error: err})
        })
    }
    /*
        Request body:
            {
	        "status": "Delivered"
            }
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
    */
    async changeOrderStatus(req: Request, res: Response, next: NextFunction) {
        await this.orderService.changeStatus(req.params.id, req.body.status).then(() => {
            res.status(200).send({message: "Order status updated successfully"})
        }).catch((err) => {
            console.log(err)
            res.status(500).send({error: err})
        })
    }

    public getRouter() {
        return this.router
    }
}