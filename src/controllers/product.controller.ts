import { Request, Response, Router, NextFunction, response } from "express";
import { ProductService } from "../services/product.services"; 
import { validJWTNeeded } from "../middleware/valid.token.needed";
import { resolveCountryCode } from "../middleware/resolve.country.code";
import { check } from "express-validator"
import { validateResult } from "../middleware/validate.result";

export class ProductController {
    private router: Router

    constructor(private productService: ProductService){
        this.initRoutes()
    }

    private initRoutes(){
        this.router = Router()
        this.router.post("/", [ 
            check('name').exists(),
            check('category').exists(),
            check('price').exists(), validateResult, validJWTNeeded, resolveCountryCode,
             (req: Request, res: Response, next: NextFunction) => { this.addProduct(req,res,next) }])
        this.router.get("/", (req: Request, res: Response, next: NextFunction) => { this.getAllProducts(req,res,next) })
        this.router.delete("/:id",[
            check('id').exists(), validateResult, validJWTNeeded,
             (req: Request, res: Response, next: NextFunction) => { this.deleteProduct(req,res,next) }])
        this.router.put("/",[
            check('id').exists(),
            check('name').exists(),
            check('category').exists(),
            check('price').exists(), validJWTNeeded, resolveCountryCode, (req: Request, res: Response, next: NextFunction) => { this.editProduct(req,res,next) }])
    }
     /*
        Add a Product
        Request Model:
            {
                "name": "Steel",
                "category": "raw",
                "price": "21.00"
            }
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
    */
    private async addProduct(req: Request, res: Response, next: NextFunction) {
        await this.productService.addProduct({...req.body}, req.body.countryCode).then((result) => {
            res.status(200).send({message: "Product added successfully"})
        }).catch((err) => {
            res.status(500).send({error: err})
        })
    }
     /*
        List all products - no authorization needed
    */
    private async getAllProducts(req: Request, res: Response, next: NextFunction) {
        await this.productService.getAll().then((result) => {
            res.status(200).send(result)
        }).catch((err) => {            
            res.status(500).send({error: err})
        })
    }
     /*
        Delete a Product
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
        Query param: ?id=productId
    */
    private async deleteProduct(req: Request, res: Response, next: NextFunction) {
        await this.productService.delete(req.params.id).then(() => {
            res.status(200).send({message: "Product deleted successfully"})
        }).catch((err) => {            
            res.status(500).send({error: err})
        })
    } 
    /*
        Edit a Product
        {
            "name": "Steel",
            "category": "raw",
            "price": "21.00"
        }
        Header:
            Key: Authorization
            Value: Bearer {access token - generated from /auth/token endpoint}
    */
    private async editProduct(req: Request, res: Response, next: NextFunction) {
        await this.productService.edit({...req.body}, req.body.countryCode).then(() => {
            res.status(200).send({message: "Product edited successfully"})
        }).catch((err) => {            
            res.status(500).send({error: err})
        })
    }

    public getRouter() {
        return this.router
    }
}