import { UserService } from "../services/user.service";
import { Request, Response, Router, NextFunction } from "express";
import { validateResult } from "../middleware/validate.result";
import { check } from "express-validator"

export class UserController{
    private router: Router

    constructor(private userService: UserService){
        this.initRouter()
    }

    initRouter(){
        this.router = Router()
        this.router.post("/",[
            check('username').exists(),
            check('password').exists(), validateResult,
            (req: Request, res: Response, next: NextFunction) => { this.registerUser(req,res,next) }])  
    }
    /*
        Register a new user
        {
            "username" : "user",
            "password" : "password"
        } 
    */
    private async registerUser(req: Request, res: Response, next: NextFunction) {
        await this.userService.addUser(req.body).then(() => {
            res.status(200).send({message: "User registered successfully"})
        }).catch((err) => {
            res.status(500).send({error: err})
        })
    }

    getRouter(){
        return this.router
    }
}