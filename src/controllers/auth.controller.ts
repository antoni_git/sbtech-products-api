import { Request, Response, Router, NextFunction } from "express";
import { verifyUser } from "../middleware/verify.user";
import { generateToken } from "../utils/generate.token";
import { UserService } from "../services/user.service"; 
import { check } from "express-validator"
import { validateResult } from "../middleware/validate.result";

export class AuthController{
    private router: Router

    constructor(private userService: UserService){
        this.initRouter()
    }

    initRouter(){
        this.router = Router()
        this.router.post("/token", [ 
            check('username').exists(),
            check('countryCode').exists(),
            check('password').exists(), validateResult , verifyUser(this.userService),
            (req: any, res: any, next: any) => { this.getToken(req, res, next) }]);
    }
   /*
        Generate an access token for the user.
        Request body:
        {
            "username" : "username",
            "password" : "password",
            "countryCode" : "BG"
        }
    */
    private async getToken(req: Request, res: Response, next: NextFunction){
        await generateToken(req.body).then((result) => {
            res.status(201).send(result)
        }).catch((err) => {
            res.status(500).send({error: err});
        })
    }    

    public getRouter(){
        return this.router;
    }
}