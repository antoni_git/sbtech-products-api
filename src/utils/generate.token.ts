import * as crypto from "crypto"
import * as jwt from "jsonwebtoken" 

export const generateToken = (body: any): Promise<any> => {
    return new Promise((resolve, reject) => {
        try{
            console.log("IN JWT");
            let refreshId = body.userId + process.env.JWT_SECRET;
            let salt = crypto.randomBytes(16).toString("base64");
            let hash = crypto.createHmac("sha512", salt).update(refreshId).digest("base64");
            body.refreshKey = salt;
            console.log( {countryCode: body.countryCode})
            let token = jwt.sign(
                {countryCode: body.countryCode},
                process.env.JWT_SECRET,
                { expiresIn: '1h'});
            let b = new Buffer(hash);
            console.log("Buffer");
            let refreshToken = b.toString("base64");
            resolve({accessToken: token, refreshToken: refreshToken})
        } catch (err){
            console.log(err)
            reject(err)
        }
    })       
} 