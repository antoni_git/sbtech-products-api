import { IProduct, ProductModel } from "../models/product";
import { VATService } from "./vat.service";

export class ProductService{
    constructor(private vATService: VATService){

    }

    public async getAll(): Promise<any>{
        return new Promise<any>((resolve, reject) => {
            ProductModel.find()
            .then((result) => {
                console.log(result)
                resolve((result as IProduct[]).map(product => Object.assign({},
                     {id:product._id, name: product.name, price: product.price, categry: product.category})));
            })
            .catch((err) => {
                reject(err)
            })
        })
    }

    public async delete(productId: string){
        ProductModel.remove({_id: productId}).catch((err) => {
            console.log(err)
        })
    }


    public async edit(newProduct: IProduct, countryCode: string){
        const vATRate = await this.vATService.getVATRate(countryCode)   
        const vat: number =  (vATRate / 100) *  newProduct.price 
        newProduct.price = newProduct.price * 1 + vat
        ProductModel.findById(newProduct.id).then((result) => {
            result = Object.assign(result, newProduct)
            result.save()
        }).catch((err) => {
            console.log(err)
        })
    }

    public async addProduct(product: IProduct, countryCode: string): Promise<void>{
        return new Promise(async (resolve, reject) => {
            const vATRate = await this.vATService.getVATRate(countryCode)   
            const vat: number =  (vATRate / 100) *  product.price 
            product.price = product.price * 1 + vat
            console.log(product)      
            const newProduct = new ProductModel({...product})
            newProduct.save().then(() => {
                resolve()
            }).catch((err) => {
                reject(err)
                console.log(err)
            })
        })
    }

    public async addProductDev(product: IProduct, countryCode: string): Promise<string>{
        return new Promise(async (resolve, reject) => {
            const vATRate = await this.vATService.getVATRate(countryCode)   
            const vat: number =  (vATRate / 100) *  product.price 
            product.price = product.price * 1 + vat
            console.log(product)      
            const newProduct = new ProductModel({...product})
            newProduct.save().then((result) => {
                resolve(result._id)
            }).catch((err) => {
                reject(err)
                console.log(err)
            })
        })
    }
}