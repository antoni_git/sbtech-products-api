import { OrderModel, IOrder } from "../models/order"; 

export class OrderService{

    public async addOrder(order: IOrder): Promise<void>{
        return new Promise((resolve, reject) => {
            const newOrder = new OrderModel({...order, date: Date()}) 
            newOrder.save().then(() => {
                resolve()
            }).catch((err) => {
                reject(err)
            }
        )})
    }


    public async addOrderDev(order: any): Promise<void>{
        return new Promise((resolve, reject) => {
            const newOrder = new OrderModel({...order, date: Date()}) 
            newOrder.save().then(() => {
                resolve()
            }).catch((err) => {
                reject(err)
            }
        )})
    }

    public async changeStatus(orderId: String, status: String): Promise<void>{
        return new Promise((resolve, reject) => {
            OrderModel.findById(orderId).then((result) => {
                console.log(result)
                console.log({status: status})
                result = Object.assign(result, {status: status})
                result.save()
                resolve()
            }).catch((err) => {
                reject(err)
            }
        )})
    }

    public async getAllOrders(): Promise<any>{
        return new Promise<any>((resolve, reject) => {
            OrderModel.find().then((result) => {
                resolve((result as IOrder[]).map(order => Object.assign({},
                    {id:order._id, date: order.date.getFullYear() + "-" + order.date.getMonth() + "-" + order.date.getDate(),
                     products: order.products, status: order.status})))
            }).catch((err) => {
                reject(err)
            })
        })
    }
}