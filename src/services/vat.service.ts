import * as request from "request" 

export class VATService { 

    getVATRate(countryCode: string): Promise<number> { 
        return new Promise<number>((resolve, reject) => {
            request("https://jsonvat.com/", { json: true }, (err, res, body) => {
                if (err) { console.log(err); }
                body.rates.forEach((rate: any) => {
                    if(rate.code === countryCode){
                        console.log(rate.periods[0].rates.standard as number)
                        resolve(rate.periods[0].rates.standard as number)
                    }                    
                }); 
                reject("Invalid country code")
            }) 
        });
    }
}