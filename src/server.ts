import * as express from "express"
import * as mongoose from "mongoose"
import { ProductController } from "./controllers/product.controller"
import { ProductService } from "./services/product.services"
import bodyParser = require("body-parser")
import { OrderControlloer } from "./controllers/order.controller"
import { OrderService } from "./services/order.service"
import { UserController } from "./controllers/user.controller"
import { AuthController } from "./controllers/auth.controller"
import { UserService } from "./services/user.service"
import { VATService } from "./services/vat.service"
import { IProduct } from "./models/product"
import { IOrder, Status } from "./models/order"
const dotenv = require("dotenv")
dotenv.config()

class Server {
    private app: express.Application

    constructor(private productController: ProductController,
      private orderController: OrderControlloer,
      private userController: UserController,
      private authController: AuthController) { 
        this.app = express()
        this.app.use(bodyParser.json())
        this.app.use("/products", this.productController.getRouter())
        this.app.use("/orders", this.orderController.getRouter())
        this.app.use("/users", this.userController.getRouter())
        this.app.use("/auth", this.authController.getRouter())
        this.connect()
        this.startServer()


        populateDb()
    }

    connect() {
        mongoose
      .connect(
        "mongodb://storage_db:27017/test",
        { useNewUrlParser: true }
      )
      .then(() => {
        return console.info(`Successfully connected to 
        "mongodb://storage_db:27017/test"`);
      })
      .catch(error => {
        console.error("Error connecting to database: ", error);
        return process.exit(1);
      });
    }
    
    startServer() {
        this.app.listen(8080, () => {
            console.log("Listening on port 8080")
        })
    }
}
const server = new Server(
  new ProductController(new ProductService(new VATService())),
   new OrderControlloer(new OrderService()),
   new UserController(new UserService()),
   new AuthController(new UserService()))

 async function populateDb() {
   const productSErvice = new ProductService(new VATService())
   const id1 = await productSErvice.addProductDev({name : "steel", price: 50, category: "raw"} as IProduct, "BG")
   const id2 = await productSErvice.addProductDev({name : "leather", price: 10, category: "raw"} as IProduct, "BG")
   const id3 = await productSErvice.addProductDev({name : "gold", price: 100, category: "raw"} as IProduct, "BG")
   const id4 = await productSErvice.addProductDev({name : "jewelery", price: 1000, category: "luxury"} as IProduct, "BG")
   const id5 = await productSErvice.addProductDev({name : "watch", price: 2000, category: "luxury"} as IProduct, "BG")
   const id6 = await productSErvice.addProductDev({name : "candy", price: 50, category: "food"} as IProduct, "BG")
   const orderService = new OrderService()
   orderService.addOrderDev({status: Status.Pending, products: [id1, id2]})
   orderService.addOrderDev({status: Status.Pending, products: [id3, id4]})
   orderService.addOrderDev({status: Status.Pending, products: [id5, id6]})
   orderService.addOrderDev({status: Status.Delivered, products: [id1, id3]})
   orderService.addOrderDev({status: Status.Delivered, products: [id1, id2]})

 }