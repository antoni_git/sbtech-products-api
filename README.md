# sbTech-products-api

How to run - docker-compose up

Api should be accessible on localhost:8080

A REST API to manage products and orders, using JWT authentication that uses a public VAT API
to get country tax rates.

Using docker-compose to run the app into a container. Also to download a mongoDb image and run it as well.

Things I would improve - 
    better error handling for some endpoints
    better validation and relationship between the products and orders table
    not use mongoDb instead use a relational SQL db but for the sake of time it was easier to setup
    add tests
    use a better seeding technique

All endpoints but get all products require a valid token.

The api asks for a country code on login to encode it into the jwt so we can use it for the VAT API call.

I've included a postman collection with all the requests for convenience


